package pl.botornot.botnot;

public class Message {
    private String Author;
    private String message;

    public Message() {
    }

    public String getAuthor() {
        return Author;
    }

    public String getMessage() {
        return message;
    }
}
