package pl.botornot.botnot;

import pl.botornot.botnot.Message;

public class MessageSnapshot {

    Message[] msg;
    String email;
    String from;

    public MessageSnapshot() {
    }

    public Message[] getMsg() {
        return msg;
    }

    public String getEmail() {
        return email;
    }

    public String getFrom() {
        return from;
    }
}
