package pl.botornot.botnot.CustomViews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.VideoView;

public class ScaledVideoView extends VideoView {

    private int mVideoWidth;
    private int mVideoHeight;
    private int mScreenWidth;
    private int mScreenHeight;
    private boolean firstRun = true;

    public ScaledVideoView(Context context) {
        super(context);
    }

    public ScaledVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScaledVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ScaledVideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setVideoSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(firstRun){
            mScreenWidth = getDefaultSize(mVideoWidth, widthMeasureSpec);
            mScreenHeight = getDefaultSize(mVideoHeight, heightMeasureSpec);
            firstRun = false;
        }

        int width = mScreenWidth;
        int height = (int)(mScreenHeight*1.2f);
        if (mVideoWidth > 0 && mVideoHeight > 0) {
            if (mVideoWidth * height < width * mVideoHeight) {
                // Log.i("@@@", "image too tall, correcting");
                height = width * mVideoHeight / mVideoWidth;
            } else if (mVideoWidth * height > width * mVideoHeight) {
                // Log.i("@@@", "image too wide, correcting");
                width = height * mVideoWidth / mVideoHeight;
            } else {
                // Log.i("@@@", "aspect ratio is correct: " +
                // width+"/"+height+"="+
                // mVideoWidth+"/"+mVideoHeight);
            }
        }
        // Log.i("@@@", "setting size: " + width + 'x' + height);
        setMeasuredDimension(width, height);
        setTranslationX(-(mVideoWidth-width)/2);
    }
}
