package pl.botornot.botnot;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        final Bundle bundle = getIntent().getExtras();
        user = bundle.getParcelable("user");

        ((TextView)findViewById(R.id.profile_name)).setText(user.getName());
        ((TextView)findViewById(R.id.profile_surname)).setText(user.getSurname());
        ((TextView)findViewById(R.id.age)).setText(user.getAge());
        ((TextView)findViewById(R.id.sport_type)).setText(user.getDiscipline());
        final Intent mainIntent = new Intent(this, Messages.class);

        ((Button)findViewById(R.id.sendbtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(mainIntent);
                finish();
            }
        });

    }
}
