package pl.botornot.botnot.Fragments;

import android.app.Activity;
import android.content.Context;
import android.support.design.internal.BottomNavigationMenu;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import pl.botornot.botnot.MainActivity;
import pl.botornot.botnot.R;

public class ProfileFragment extends Fragment {

    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_main_profile, container, false);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser mCurrentUser = mAuth.getCurrentUser();
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser.getUid());

        rootView.findViewById(R.id.profile_container).setPadding(0,0,0,(int)getResources().getDimension(R.dimen.nav_bar_height));

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ((TextView)rootView.findViewById(R.id.profile_name)).setText(dataSnapshot.child("name").getValue().toString());
                ((TextView)rootView.findViewById(R.id.profile_surname)).setText(dataSnapshot.child("surname").getValue().toString());
                ((TextView)rootView.findViewById(R.id.age)).setText(dataSnapshot.child("age").getValue().toString());
                ((TextView)rootView.findViewById(R.id.sport_type)).setText(dataSnapshot.child("discipline").getValue().toString());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        return rootView;
    }
}
