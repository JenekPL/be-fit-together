package pl.botornot.botnot.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import pl.botornot.botnot.MainActivity;
import pl.botornot.botnot.ProfileActivity;
import pl.botornot.botnot.R;
import pl.botornot.botnot.User;

public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener, GoogleMap.OnMarkerClickListener {


    class Nearby {
        public LatLng location;
        public String displayName;
        public String key;

        Nearby(LatLng location, String displayName, String key) {
            this.location = location;
            this.displayName = displayName;
            this.key = key;
        }
    }

    private final static int LOCATION_PERMISSION_REQUEST_CODE = 1;

    GoogleMap map;
    LocationManager locationManager;
    MapView mapView;
    boolean isFirstRun = true;
    List<Nearby> usersList = new ArrayList<>();
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mDatabase;

    public static MapFragment newInstance() {

        Bundle args = new Bundle();

        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_main_map, container, false);

        mapView = rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        final String uid = mCurrentUser.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            if (!dataSnapshot1.getKey().equals(uid)){
                                double latitude = Double.parseDouble(dataSnapshot1.child("latitude").getValue().toString());
                                double longitude = Double.parseDouble(dataSnapshot1.child("longitude").getValue().toString());
                                usersList.add(new Nearby(new LatLng(latitude, longitude), dataSnapshot1.child("name").getValue().toString() + " " + dataSnapshot1.child("surname").getValue().toString(), dataSnapshot1.getKey()));

                            }

                        }




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLocationChanged(Location location) {
        map.clear();
        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

        double latitude = location.getLatitude();
        String latitude1 = String.valueOf(latitude);
        double longitude = location.getLongitude();
        String longitude1 = String.valueOf(longitude);

        final String uid = mCurrentUser.getUid();

        mDatabase.child(uid).child("latitude").setValue(latitude1);
        mDatabase.child(uid).child("longitude").setValue(longitude1);

        for (Nearby n : usersList) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(n.location);
            markerOptions.title(n.displayName);
            map.addMarker(markerOptions);
        }

        if (isFirstRun) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 17.0f));
            isFirstRun = false;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMarkerClickListener(this);
        tryLocalizeDevice();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE:
                tryLocalizeDevice();
        }
    }


    void tryLocalizeDevice() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
            map.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        for (Nearby n : usersList) {
            if (n.location.equals(marker.getPosition())) {
/*
                Intent mainIntent = new Intent(getActivity(), ProfileActivity.class);
                Bundle b = new Bundle();
                User xd = n.snap.toObject(User.class);
                b.putParcelable("user", xd);
                mainIntent.putExtras(b);
                getActivity().startActivityForResult(mainIntent, MainActivity.PROFILE_ACTIVITY);
                return true;
            }

        }

        return false;
    }*/
            }
        }
        return false;
    }
}

