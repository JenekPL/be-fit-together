package pl.botornot.botnot;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import pl.botornot.botnot.Fragments.ChatFragment;
import pl.botornot.botnot.Fragments.MapFragment;
import pl.botornot.botnot.Fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity {

    public final static int SEND_MESSAGE = 0;
    public final static int BACK = 1;
    public final static int PROFILE_ACTIVITY = 1;

    private ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private BottomNavigationView mNavigation;
    boolean mIsChanged = false;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser mCurrentUser;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_chat:
                    if(!mIsChanged) mViewPager.setCurrentItem(0, true);
                    else mIsChanged = false;
                    return true;
                case R.id.navigation_map:
                    if(!mIsChanged) mViewPager.setCurrentItem(1, true);
                    else mIsChanged = false;
                    return true;
                case R.id.navigation_profile:
                    if(!mIsChanged) mViewPager.setCurrentItem(2, true);
                    else mIsChanged = false;
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        String uid = mCurrentUser.getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


        mNavigation = findViewById(R.id.navigation);
        mNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                mIsChanged = true;
                switch (position) {
                    case 0:
                        mNavigation.setSelectedItemId(R.id.navigation_chat);
                        break;
                    case 1:
                        mNavigation.setSelectedItemId(R.id.navigation_map);
                        break;
                    case 2:
                        mNavigation.setSelectedItemId(R.id.navigation_profile);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });

        mIsChanged = true;
        mNavigation.setSelectedItemId(R.id.navigation_map);
        mViewPager.setCurrentItem(1, true);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm) { super(fm); }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ChatFragment.newInstance();
                case 1:
                    return MapFragment.newInstance();
                case 2:
                    return ProfileFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == PROFILE_ACTIVITY) {
            if (resultCode == SEND_MESSAGE) {
                mIsChanged = true;
                mNavigation.setSelectedItemId(R.id.navigation_chat);
                mViewPager.setCurrentItem(0, true);
            }
        }
    }
}
