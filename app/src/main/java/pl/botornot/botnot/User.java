package pl.botornot.botnot;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;

public class User implements Parcelable {
    private String age;
    private String discipline;
    private String email;
    private String latitude;
    private String longtitude;
    private String name;
    private String password;
    private String surname;

    public User(){}

    protected User(Parcel in) {
        age = in.readString();
        discipline = in.readString();
        email = in.readString();
        latitude = in.readString();
        longtitude = in.readString();
        name = in.readString();
        password = in.readString();
        surname = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getEmail(){ return email; }

    public String getAge() {
        return age;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public String getDiscipline() { return discipline; }

    public String getName() {return name;}

    public String getSurname() { return surname; }

    public String getPassword(){return password;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.age);
        dest.writeString(this.discipline);
        dest.writeString(this.email);
        dest.writeString(this.latitude);
        dest.writeString(this.longtitude);
        dest.writeString(this.name);
        dest.writeString(this.password);
        dest.writeString(this.surname);
    }
}
